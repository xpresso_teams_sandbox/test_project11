""" Factory class for database connectors in Data-Connectivity """

__author__ = 'Shlok Chaudhari'
__all__ = 'connector'

import enum
import xpresso.ai.core.commons.exceptions.xpr_exceptions as xpr_exp
from xpresso.ai.core.data.connections.external import PrestoDBConnector
from xpresso.ai.core.data.connections.external import GoogleBigQueryDBConnector


class DBType(enum.Enum):
    """

    Enum Class that lists types of DBMS supported by DBConnector class

    """

    Presto = "presto"
    BigQuery = "bigquery"


class DBConnectorFactory:
    """

    Factory class to provide Connector object of specified type

    """

    @staticmethod
    def getconnector(datasource_type):
        """

        This method returns Connector object of a specific datasource

        Args:
            datasource_type (str): a string object stating the
                datasource

        Returns:
            object: Connector object
        """

        if datasource_type is None:
            return PrestoDBConnector()
        elif datasource_type.lower() == DBType.BigQuery.value:
            return GoogleBigQueryDBConnector()
        elif datasource_type.lower() == DBType.Presto.value:
            return PrestoDBConnector()
        else:
            raise xpr_exp.InvalidDictionaryException
